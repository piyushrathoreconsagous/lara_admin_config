<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\MediaUploadingTrait;    // to upload media  
use App\Http\Requests\MassDestroyhomesliderRequest;
use App\Http\Requests\StoreHomeSliderRequest;       // store request validation
use App\Http\Requests\UpdateHomeSliderRequest;        
use App\HomeSlider;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class HomesliderController extends Controller
{   
    use MediaUploadingTrait;    // to upload media  

    public function index()
    {
        abort_if(Gate::denies('homeslider_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $homeslider = HomeSlider::all();

        return view('admin.homeslider.index', compact('homeslider'));
    }

    public function create()
    {
        abort_if(Gate::denies('homeslider_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        return view('admin.homeslider.create');
    }

    public function store(StoreHomeSliderRequest $request)
    {
        $homeslider = HomeSlider::create($request->all());

        if ($request->input('photo', false)) {
          $res =  $homeslider->addMedia(storage_path('tmp/uploads/' . $request->input('photo')))->toMediaCollection('photo');
        }

        return redirect()->route('admin.homeslider.index');
    }

    public function edit(HomeSlider $homeslider)
    {
        abort_if(Gate::denies('homeslider_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        return view('admin.homeslider.edit', compact('homeslider'));
    }

    public function update(UpdateHomeSliderRequest $request, HomeSlider $homeslider)
    {
        $homeslider->update($request->all());

        if ($request->input('photo', false)) {
            if (!$homeslider->photo || $request->input('photo') !== $homeslider->photo->file_name) {
                $homeslider->addMedia(storage_path('tmp/uploads/' . $request->input('photo')))->toMediaCollection('photo');
            }
        } elseif ($homeslider->photo) {
            $homeslider->photo->delete();
        }

        return redirect()->route('admin.homeslider.index');
    }

    public function show(HomeSlider $homeslider)
    { 
        abort_if(Gate::denies('homeslider_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        return view('admin.homeslider.show', compact('homeslider'));
    }

    public function destroy(HomeSlider $homeslider)   // HomeSlider $homeslider both name must same
    {    
        abort_if(Gate::denies('homeslider_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        $homeslider->delete();
        return back();
    }

    public function massDestroy(MassDestroyhomesliderRequest $request)
    {  
        HomeSlider::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
