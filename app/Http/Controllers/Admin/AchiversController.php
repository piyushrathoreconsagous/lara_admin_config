<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\MediaUploadingTrait;    // to upload media  
use App\Http\Requests\MassDestroyTopAchiversRequest;
use App\Http\Requests\StoreTopAchiversRequest;       // store request validation
use App\Http\Requests\UpdateTopAchiversRequest;        
use App\TopAchivers;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class AchiversController extends Controller
{   
    use MediaUploadingTrait;    // to upload media  

    public function index()
    {
        abort_if(Gate::denies('achivers_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $achivers = TopAchivers::all();

        return view('admin.achivers.index', compact('achivers'));
    }

    public function create()
    {
        abort_if(Gate::denies('achivers_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        return view('admin.achivers.create');
    }

    public function store(StoreTopAchiversRequest $request)
    {
        $topachivers = TopAchivers::create($request->all());

        if ($request->input('photo', false)) {
          $res =  $topachivers->addMedia(storage_path('tmp/uploads/' . $request->input('photo')))->toMediaCollection('photo');
        }

        return redirect()->route('admin.achivers.index');
    }

    public function edit(TopAchivers $topachivers)
    {
        abort_if(Gate::denies('achivers_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        return view('admin.achivers.edit', compact('topachivers'));
    }

    public function update(UpdateHomeSliderRequest $request, HomeSlider $homeslider)
    {
        $homeslider->update($request->all());

        if ($request->input('photo', false)) {
            if (!$homeslider->photo || $request->input('photo') !== $homeslider->photo->file_name) {
                $homeslider->addMedia(storage_path('tmp/uploads/' . $request->input('photo')))->toMediaCollection('photo');
            }
        } elseif ($homeslider->photo) {
            $homeslider->photo->delete();
        }

        return redirect()->route('admin.achivers.index');
    }

    public function show(TopAchivers $topachivers)
    {   
        abort_if(Gate::denies('achivers_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        return view('admin.achivers.show', compact('topachivers'));
    }

    public function destroy(TopAchivers $topachivers)   // HomeSlider $homeslider both name must same
    {    
        abort_if(Gate::denies('achivers_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        $topachivers->delete();
        return back();
    }

    public function massDestroy(MassDestroyhomesliderRequest $request)
    {  
        TopAchivers::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
