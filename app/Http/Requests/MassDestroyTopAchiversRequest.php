<?php

namespace App\Http\Requests;

use App\TopAchivers;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Response;

class MassDestroyTopAchiversRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('achivers_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;
    }

    public function rules()
    {
        return [
            'ids'   => 'required|array',
            'ids.*' => 'exists:top_achivers,id',     // table name, column name
        ];
    }
}
