<?php

namespace App\Http\Requests;

use App\TopAchivers;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Response;

class UpdateTopAchiversRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('achivers_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;
    }

    public function rules()
    {
        return [
            'title'   => [
                'required',
            ],
            'description' => [
                'required',
            ],
        ];
    }
}
