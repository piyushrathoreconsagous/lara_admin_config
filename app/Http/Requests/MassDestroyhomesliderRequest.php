<?php

namespace App\Http\Requests;

use App\HomeSlider;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Response;

class MassDestroyhomesliderRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('homeslider_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;
    }

    public function rules()
    {
        return [
            'ids'   => 'required|array',
            'ids.*' => 'exists:home_slider,id',     // table name, column name
        ];
    }
}
