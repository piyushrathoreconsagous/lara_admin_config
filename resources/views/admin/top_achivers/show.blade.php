@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.show') }} {{ trans('cruds.homeSlider.title') }}
    </div>

    <div class="card-body">
        <div class="mb-2">
            <table class="table table-bordered table-striped">
                <tbody>
                    <tr>
                        <th>
                            {{ trans('cruds.homeSlider.fields.id') }}
                        </th>
                        <td>
                            {{ $homeslider->id }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.homeSlider.fields.title') }}
                        </th>
                        <td>
                            {{ $homeslider->title }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.homeSlider.fields.photo') }}
                        </th>
                        <td>
                            @if($homeslider->photo)
                                <a href="{{ asset($homeslider->photo->getUrl()) }}" target="_blank">
                                    <img src="{{ asset($homeslider->photo->getUrl('thumb')) }}" width="50px" height="50px">
                                </a>
                            @endif
                        </td>
                    </tr>

                  
                </tbody>
            </table>
            <a style="margin-top:20px;" class="btn btn-default" href="{{ url()->previous() }}">
                {{ trans('global.back_to_list') }}
            </a>
        </div>


    </div>
</div>
@endsection