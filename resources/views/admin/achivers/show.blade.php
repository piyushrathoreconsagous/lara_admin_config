@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.show') }} {{ trans('cruds.topAchivers.title') }}
    </div>
<?php  print_r($topachivers) ?>
    <div class="card-body">
        <div class="mb-2">
            <table class="table table-bordered table-striped">
                <tbody>
                    <tr>
                        <th>
                            {{ trans('cruds.topAchivers.fields.id') }}
                        </th>
                        <td>
                            {{ $topachivers->id }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.topAchivers.fields.title') }}
                        </th>
                        <td>
                            {{ $topachivers->title }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.topAchivers.fields.photo') }}
                        </th>
                        <td>
                            @if($topachivers->photo)
                                <a href="{{ asset($topachivers->photo->getUrl()) }}" target="_blank">
                                    <img src="{{ asset($topachivers->photo->getUrl('thumb')) }}" width="50px" height="50px">
                                </a>
                            @endif
                        </td>
                    </tr>

                  
                </tbody>
            </table>
            <a style="margin-top:20px;" class="btn btn-default" href="{{ url()->previous() }}">
                {{ trans('global.back_to_list') }}
            </a>
        </div>


    </div>
</div>
@endsection