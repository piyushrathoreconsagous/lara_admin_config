@extends('layouts.admin')
@section('content')
@can('achivers_create')
    <div style="margin-bottom: 10px;" class="row">
        <div class="col-lg-12">
            <a class="btn btn-success" href="{{ route("admin.achivers.create") }}">
                {{ trans('global.add') }} {{ trans('cruds.topAchivers.title_singular') }}
            </a>
        </div>
    </div>
@endcan
<div class="card">
    <div class="card-header">
        {{ trans('cruds.topAchivers.title_singular') }} {{ trans('global.list') }}
    </div>

    <div class="card-body">
        <div class="table-responsive">
            <table class=" table table-bordered table-striped table-hover datatable datatable-Permission">
                <thead>
                    <tr>
                        <th width="10">

                        </th>
                        <th>
                            {{ trans('cruds.topAchivers.fields.id') }}
                        </th>
                        <th>
                            {{ trans('cruds.topAchivers.fields.photo') }}
                        </th>
                        <th>
                            {{ trans('cruds.topAchivers.fields.title') }}
                        </th>
                        <th>
                            {{ trans('cruds.topAchivers.fields.description') }}
                        </th>
                        <th>
                            &nbsp;
                        </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($achivers as $key => $list) 

                        
                        <tr data-entry-id="{{ $list->id }}">
                            <td>

                            </td>
                            <td>
                                {{ $list->id ?? '' }}
                            </td>
                            <td>
                                 @if($list->photo)
                                    <a href="{{ asset($list->photo->getUrl()) }}" target="_blank">
                                        <img src="{{ asset($list->photo->getUrl('thumb')) }}" width="50px" height="50px">
                                    </a>
                                @endif                                
                            </td>
                            <td>
                                {{ $list->title ?? '' }}
                            </td>
                            <td>
                                {{ $list->description ?? '' }}
                            </td>
                            <td>
                                @can('achivers_show')
                                    <a class="btn btn-xs btn-primary" href="{{ route('admin.achivers.show', $list->id) }}">
                                        {{ trans('global.view') }}
                                    </a>
                                @endcan

                                @can('achivers_edit')
                                    <a class="btn btn-xs btn-info" href="{{ route('admin.achivers.edit', $list->id) }}">
                                        {{ trans('global.edit') }}
                                    </a>
                                @endcan

                                @can('achivers_delete')
                                    <form action="{{ route('admin.achivers.destroy', $list->id) }}" method="POST" onsubmit="return confirm('{{ trans('global.areYouSure') }}');" style="display: inline-block;">                                          
                                        <input type="hidden" name="_method" value="DELETE">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <input type="submit" class="btn btn-xs btn-danger" value="{{ trans('global.delete') }}">
                                    </form>
                                @endcan

                            </td>

                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
@section('scripts')
@parent
<script>
    $(function () {
  let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
@can('homeslider_delete')
  let deleteButtonTrans = '{{ trans('global.datatables.delete') }}'
  let deleteButton = {
    text: deleteButtonTrans,
    url: "{{ route('admin.homeslider.massDestroy') }}",
    className: 'btn-danger',
    action: function (e, dt, node, config) {
      var ids = $.map(dt.rows({ selected: true }).nodes(), function (entry) {
          return $(entry).data('entry-id')
      });

      if (ids.length === 0) {
        alert('{{ trans('global.datatables.zero_selected') }}')

        return
      }

      if (confirm('{{ trans('global.areYouSure') }}')) {
        $.ajax({
          headers: {'x-csrf-token': _token},
          method: 'POST',
          url: config.url,
          data: { ids: ids, _method: 'DELETE' }})
          .done(function () { location.reload() })
      }
    }
  }
  dtButtons.push(deleteButton)
@endcan

  $.extend(true, $.fn.dataTable.defaults, {
    order: [[ 1, 'asc' ]],
    pageLength: 100,
  });
  $('.datatable-Permission:not(.ajaxTable)').DataTable({ buttons: dtButtons })
    $('a[data-toggle="tab"]').on('shown.bs.tab', function(e){
        $($.fn.dataTable.tables(true)).DataTable()
            .columns.adjust();
    });
})

</script>
@endsection